"""
CGCC-5003 Lab 1
Author: Daniel Allison

Program purpose:
This program exists to take 5 numbers provided from user input, compute their mean and standard deviation, and display
them back to the user.

Program execution flow:
First, input is taken from the user after prompting them to enter a number. This is done 5 times and each is cast to a
float before being saved to different variables. Next, the average is computed and saved to a variable named average
using the avg5nums function which sums the 5 arguments it is provided, in this case the inputs taken earlier, and then
divides the sum by 5. This average is then shown to the user. Then the standard deviation is computed using a function
called standardDeviation5nums which takes an average and 5 numbers as parameters, and the values from earlier are used.
The function then uses another function called diffFromAvgSquared to calculate temporary values which are fed into
avg5nums and the square root of the result of avg5nums is returned as the result of standardDeviation5nums. This is then
printed to the user.
"""

# Inputs are stored for later use after being cast to float type
firstNum = float(input("Enter first number: "))
secondNum = float(input("Enter second number: "))
thirdNum = float(input("Enter third number: "))
fourthNum = float(input("Enter fourth number: "))
fifthNum = float(input("Enter fifth number: "))

# Calculates the average number of 5 numbers
def avg5nums(firstNum, secondNum, thirdNum, fourthNum, fifthNum):
    return (firstNum + secondNum + thirdNum + fourthNum + fifthNum)/5

average = avg5nums(firstNum, secondNum, thirdNum, fourthNum, fifthNum)

# Show the average to the user
print("The average is: " + str(average))

# Finds the difference from the average and squares it
def diffFromAvgSquared(average, number):
    return (number - average)**2

# Calculates standard deviation of 5 numbers
def standardDeviation5nums(average, firstNum, secondNum, thirdNum, fourthNum, fifthNum):
    tmp1 = diffFromAvgSquared(average, firstNum)
    tmp2 = diffFromAvgSquared(average, secondNum)
    tmp3 = diffFromAvgSquared(average, thirdNum)
    tmp4 = diffFromAvgSquared(average, fourthNum)
    tmp5 = diffFromAvgSquared(average, fifthNum)

    return (avg5nums(tmp1, tmp2, tmp3, tmp4, tmp5))**(1/2)

# Show the standard deviation to the user
print("The standard deviation is: " + str(standardDeviation5nums(average, firstNum, secondNum, thirdNum, fourthNum, fifthNum)))